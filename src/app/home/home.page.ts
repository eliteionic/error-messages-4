import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private myForm: FormGroup;
  public customNameErrors = {
    'required': 'What do you expect people to call you without a name?',
    'maxlength': 'You are going to need a shorter username'
  }

  constructor(private formBuilder: FormBuilder){
    this.myForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])]
    });
  }

}
